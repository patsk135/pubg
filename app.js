import express, { json, urlencoded } from 'express'
import cookieParser from 'cookie-parser'
import dotenv from 'dotenv'
import morgan from 'morgan'
import rootRouter from './routes'
import session from 'express-session'
import serveStatic from 'serve-static'
import { join } from 'path'
import { intializeDatabase } from './db/db'

dotenv.config()

const app = express()
const port = process.env.PORT

var server = require('http').Server(app)
var io = require('socket.io')(server)
server.listen(port)
intializeDatabase()

app.use(json())
app.use(urlencoded({ extended: true }))
app.use(cookieParser())
app.use(morgan('dev'))
app.use((req, res, next) => {
	req.io = io
	next()
})
app.use(
	session({
		secret: 'this is a secret',
		resave: true,
		saveUninitialized: false
	})
)

app.use('/', serveStatic(join(__dirname, '/dist')))
app.use('/api', rootRouter)

app.get('*', function(req, res) {
	res.sendFile(__dirname + '/dist/index.html')
})

io.on('connection', client => {
	console.log('user connected')

	client.on('disconnect', () => {
		console.log('user disconnected')
	})
})

console.log(`running on port ${port}`)
