import mongoose, { Schema } from 'mongoose'
import { hash, compare } from 'bcrypt'
const UserSchema = new Schema({
	username: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	password: {
		type: String,
		required: true
	}
})

UserSchema.methods.authenticate = function(password) {
	const user = this
	return compare(password, user.password)
}

UserSchema.pre('save', function(next) {
	const user = this
	hash(user.password, 10, (err, hash) => {
		if (err) {
			console.log(err)
			return next(err)
		} else {
			user.password = hash
			next()
		}
	})
})

export const User = mongoose.model('User', UserSchema)
