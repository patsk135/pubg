import { Router } from 'express'
import { Item } from '../model/item.model'
import { requireslogin } from '../middlewares/authentication'
import { from } from 'rxjs'
import R from 'ramda'
import { mergeAll, mergeMap } from 'rxjs/operators'
const router = Router()
let io

// helper functions

const updateObservable = array =>
	from(array)
		.pipe(mergeAll())
		.subscribe(R.always(), console.log, update)

const doThis = R.compose(updateObservable, R.map)

const reset = obj => updatePrice(obj._id)(obj.originalPrice)

const sell = obj => updatePrice(obj._id)(calculatedPrice(obj.currentPrice, 0.8))

const promote = obj => updatePrice(obj._id)(calculatedPrice(obj.currentPrice, 1.5))

const updatePrice = id => newPrice =>
	Item.findByIdAndUpdate(id, { currentPrice: newPrice })

const round = R.compose(
	R.multiply(10),
	Math.ceil,
	R.multiply(0.1)
)

const calculatedPrice = R.compose(round, R.multiply)

const update = () => {
	io.sockets.emit('update')
}

//routes

router.get('/', (req, res) => {
	from(Item.find()).subscribe(data => res.send(data))
})

router.get('/calibrate', (req, res) => {
	const parse = R.map(R.evolve({ currentPrice: Number, originalPrice: Number }))
	const update = obj =>
		Item.update({ _id: obj._id }, R.pick(['currentPrice', 'originalPrice'], obj))
	const calibrate = R.compose(R.map(update), parse)

	from(Item.find())
		.pipe(mergeMap(calibrate), mergeAll())
		.subscribe(data => { }, console.log, () => {
			res.send('calibrated')
		})
})

router.use(
	(req, res, next) => {
		io = req.io
		next()
	},
	requireslogin,
)

router.post('/sell', (req, res) => {
	try {
		doThis(sell, req.body)
		res.send()
	} catch (err) {
		console.log(err)
		res.status(400).send()
	}
})

router.post('/reset', (req, res) => {
	try {
		doThis(reset, req.body)
		res.send()
	} catch (err) {
		res.status(400).send(err)
	}
})

router.post('/promote', (req, res) => {
	try {
		doThis(promote, req.body)
		res.send()
	} catch (err) {
		res.status(400).send(err)
	}
})

export default router
